﻿using System;

namespace Snakepp.design.model.game
{
    interface IEffect
    {
        void InstantaneousEffect(ISnake target);

        void ExpirationEffect(IField field);

        void AttachSnake(ISnake snake);

        ISnake GetAttachedSnake();
        
        Nullable<long> GetEffectDuration();

        void IncrementDuration(long duration);

        int GetComboCounter();

        void Run();
    }
}
