﻿namespace Snakepp.design.model.game
{
    interface IGameModel
    {
        IField getField();

        IGameRules getGameRules();
    }
}
