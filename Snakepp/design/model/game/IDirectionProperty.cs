﻿namespace Snakepp.design.model.game
{
    interface IDirectionProperty
    {
        Direction GetDirection();

        bool SetDirection(Direction direction);

        void SetReverseDirection(bool reverse);

        bool IsDirectionReversed();

        bool ForceDirection(Direction direction);

        void AllowChangeDirection();

        bool HasNextValidDirection();

        Direction GetNextValidDirection();
    }
}
