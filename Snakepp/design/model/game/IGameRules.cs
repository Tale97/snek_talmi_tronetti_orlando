﻿using System.Collections.Generic;

namespace Snakepp.design.model.game
{
    interface IGameRules
    {
        IWinConditions GetWinConditions();

        ILossConditions GetLossConditions();

        List<IItemRule> GetItemRules();

        long GetInitialSnakeDelta();

        double GetInitialSnakeMultiplier();

        long GetInitialTime();

        bool IsTimeGoingForward();
    }
}
