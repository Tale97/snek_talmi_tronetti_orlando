﻿using System.Collections.Generic;
using System.Drawing;

namespace Snakepp.design.model.game
{
    interface IField
    {
        void Begin();

        List<IItem> GetEliminatedItems();

        int GetWidth();

        int GetHeight();

        List<ICollidable> GetCell(Point point);

        List<IItem> GetItems();

        bool AddItem(IItem item);

        bool RemoveItem(IItem item);

        List<IWall> GetWalls();

        bool AddWall(IWall wall);

        List<IBodyPart> GetBodyParts();

        bool AddBodyPart(IBodyPart bodyPart);

        bool RemoveBodyPart(IBodyPart bodyPart);

        List<ISnake> GetSnakes();

        bool AddSnake(ISnake snake);

        ISnake RemoveSnake(int i);
    }
}
