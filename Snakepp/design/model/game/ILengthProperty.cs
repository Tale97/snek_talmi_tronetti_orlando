﻿namespace Snakepp.design.model.game
{
    interface ILengthProperty
    {
        int GetLength();

        void Lengthen(int n);

        void Shorten(int n);
    }
}
