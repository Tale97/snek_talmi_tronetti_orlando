﻿using System.Collections.Generic;

namespace Snakepp.design.model.game
{
    interface ILossConditions
    {
        bool CheckSnakes(List<ISnake> snakes);

        bool CheckTime(long time);
    }
}
