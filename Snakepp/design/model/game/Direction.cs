﻿namespace Snakepp.design.model.game
{
    enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
