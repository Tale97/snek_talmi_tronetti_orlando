﻿using System;

namespace Snakepp.design.model.game
{
    interface IPlayer
    {
        PlayerNumber GetPlayerNumber();

        String GetName();

        void AddScore(int score);

        void ReduceScore(int score);

        void ApplyScoreMultiplier(double mult);

        double GetScoreMultiplier();

        int GetScore();
    }
}
