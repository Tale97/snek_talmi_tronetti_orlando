﻿using System.Collections.Generic;

namespace Snakepp.design.model.game
{
    interface IWinConditions
    {
        bool CheckSnakeLength(List<ISnake> snakes);

        bool CheckScore(List<ISnake> snakes);

        bool CheckTime(long time);
    }
}
