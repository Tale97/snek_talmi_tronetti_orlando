﻿using System;

namespace Snakepp.design.model.game
{
    interface IItem
    {
        Type GetEffectType();

        void Run();
    }
}
