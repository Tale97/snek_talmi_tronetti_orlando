﻿namespace Snakepp.design.model.game
{
    interface IBodyPart : ICollidable
    {
        bool IsHead();

        void SetHead(bool head);

        bool IsBody();

        void SetBody(bool body);

        bool IsTail();

        void SetTail(bool tail);

        bool IsCombinedOnTop();

        void SetCombinedOnTop(bool combined);

        bool IsCombinedOnBottom();

        void SetCombinedOnBottom(bool combined);

        bool IsCombinedOnLeft();

        void SetCombinedOnLeft(bool combined);

        bool IsCombinedOnRight();

        void SetCombinedOnRight(bool combined);

        ISnake GetOwner();
    }
}
