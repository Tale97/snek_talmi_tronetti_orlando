﻿namespace Snakepp.design.model.game
{
    interface ISpeedProperty
    {
        long GetDeltaT();

        void SetDeltaT(long deltaT);

        void ApplySpeedMultiplier(double mul);

        double GetSpeedMultiplier();

        void ApplyLengthSpeedValue(double val);

        double GetLengthSpeedValue();
    }
}
