﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.design.model.game
{
    interface IItemRule
    {
        Type GetEffectType();

        long GetSpawnDelta();

        double GetSpawnChance();

        int GetMax();

        Nullable<long> GetItemDuration();

        Nullable<long> GetEffectDuration();
    }
}
