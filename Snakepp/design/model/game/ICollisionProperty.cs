﻿namespace Snakepp.design.model.game
{
    interface ICollisionProperty
    {
        void SetInvincibility(bool inv);

        bool IsInvincible();

        void SetIntangibility(bool intangibility);

        bool IsIntangible();

        void SetSpring(bool spring);

        bool IsSpring();
    }
}
