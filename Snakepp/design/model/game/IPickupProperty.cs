﻿namespace Snakepp.design.model.game
{
    interface IPickupProperty
    {
        void SetPickupRadius(int radius);

        int GetPickupRadius();
    }
}
