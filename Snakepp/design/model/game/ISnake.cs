﻿using System.Collections.Generic;

namespace Snakepp.design.model.game
{

    interface ISnake
    {
        IPlayer GetPlayer();

        IProperties GetProperties();

        void AddEffect(IEffect effect);

        bool RemoveEffect(IEffect effect);

        List<IEffect> GetEffects();

        bool IsAlive();

        void Kill();

        void Reverse();

        List<IBodyPart> GetBodyParts();

        bool HasMoved();
    }
}
