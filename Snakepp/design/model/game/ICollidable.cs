﻿using System.Drawing;

namespace Snakepp.design.model.game
{
    interface ICollidable
    {
        void OnCollision(ISnake collider);

        Point GetPoint();

        void SetPoint(Point point);
    }
}
