﻿namespace Snakepp.design.model.game
{
    interface IProperties
    {
        ILengthProperty GetLengthProperty();

        IDirectionProperty GetDirectionProperty();

        IPickupProperty GetPickupProperty();

        ICollisionProperty GetCollisionProperty();

        ISpeedProperty GetSpeedProperty();
    }
}
