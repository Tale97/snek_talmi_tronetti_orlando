﻿using Snakepp.design.model.game;
using System.Collections.Generic;

namespace Snakepp.implmentation.model.game.rules
{
    class GameRules : IGameRules
    {
        private readonly IWinConditions win;
        private readonly ILossConditions loss;
        private readonly List<IItemRule> itemRules;
        private readonly long initialSnakeDelta, initialTime;
        private readonly double initialSnakeMultiplier;
        private readonly bool timeGoingForward;

        public GameRules (IWinConditions win, ILossConditions loss, List<IItemRule> itemRules,
            long initialSnakeDelta, double initialSnakeMultiplier,
            long initialTime, bool timeGoingForward)
        {
            this.win = win;
            this.loss = loss;
            this.itemRules = itemRules;
            this.initialSnakeDelta = initialSnakeDelta;
            this.initialSnakeMultiplier = initialSnakeMultiplier;
            this.initialTime = initialTime;
            this.timeGoingForward = timeGoingForward;

        }

        public long GetInitialSnakeDelta()
        {
            return initialSnakeDelta;
        }

        public double GetInitialSnakeMultiplier()
        {
            return initialSnakeMultiplier;
        }

        public long GetInitialTime()
        {
            return initialTime;
        }

        public List<IItemRule> GetItemRules()
        {
            return itemRules;
        }

        public ILossConditions GetLossConditions()
        {
            return loss;
        }

        public IWinConditions GetWinConditions()
        {
            return win;
        }

        public bool IsTimeGoingForward()
        {
            return timeGoingForward;
        }
    }
}
