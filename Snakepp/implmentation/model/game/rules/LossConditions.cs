﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;

namespace Snakepp.implmentation.model.game.rules
{
    class LossConditions : ILossConditions
    {
        private readonly bool checkAllSnakesDied, timeGoesForward;
        private readonly Nullable<long> gameTime;

        public LossConditions(bool checkAllSnakesDied, Nullable<long> gameTime, bool timeGoesForward)
        {
            if (gameTime.HasValue && gameTime.Value < 0)
            {
                throw new Exception();
            }
            this.checkAllSnakesDied = checkAllSnakesDied;
            this.gameTime = gameTime;
            this.timeGoesForward = timeGoesForward;
        }

        public bool CheckSnakes(List<ISnake> snakes)
        {
            if (checkAllSnakesDied)
            {
                foreach (ISnake s in snakes)
                {
                    if (s.IsAlive())
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public bool CheckTime(long time)
        {
            if (gameTime.HasValue)
            {
                if (timeGoesForward)
                {
                    return gameTime.Value <= time;
                }
                else
                {
                    return gameTime.Value >= time;
                }
            }
            return false;
        }
    }
}
