﻿using Snakepp.design.model.game;
using System;

namespace Snakepp.implmentation.model.game.rules
{
    class ItemRule : IItemRule
    {
        private readonly Type effectType;
        private readonly long spawnDelta;
        private readonly double spawnChance;
        private readonly int max;
        private readonly Nullable<long> itemDuration, effectDuration;

        public ItemRule(long spawnDelta, double spawnChance, int max,
            Nullable<long> itemDuration, Nullable<long> effectDuration)
        {
            if (spawnDelta <= 0L || spawnChance <= 0 || max <= 0)
            {
                throw new Exception();
            }
            if (itemDuration.HasValue && itemDuration.Value <= 0)
            {
                throw new Exception();
            }
            if (effectDuration.HasValue && effectDuration.Value <= 0)
            {
                throw new Exception();
            }
        }

        public Nullable<long> GetEffectDuration()
        {
            return effectDuration;
        }

        public Type GetEffectType()
        {
            return effectType;
        }

        public Nullable<long> GetItemDuration()
        {
            return effectDuration;
        }

        public int GetMax()
        {
            return max;
        }

        public double GetSpawnChance()
        {
            return spawnChance;
        }

        public long GetSpawnDelta()
        {
            return spawnDelta;
        }
    }
}
