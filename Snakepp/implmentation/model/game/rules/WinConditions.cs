﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;

namespace Snakepp.implmentation.model.game.rules
{
    class WinConditions : IWinConditions
    {
        private readonly Nullable<int> snakeLength, scoreToReach;
        private readonly Nullable<long> timeToReach;
        private readonly bool timeGoesForward;

        public WinConditions(Nullable<int> snakeLength, Nullable<int> score, Nullable<long> time, bool timeGoesForward)
        {
            if (snakeLength.HasValue && snakeLength.Value < 0)
            {
                throw new Exception();
            }
            if (score.HasValue && score.Value < 0)
            {
                throw new Exception();
            }
            if (time.HasValue && time.Value < 0)
            {
                throw new Exception();
            }
            this.snakeLength = snakeLength;
            this.scoreToReach = score;
            this.timeToReach = time;
            this.timeGoesForward = timeGoesForward;
        }

        public bool CheckScore(List<ISnake> snakes)
        {
            if (scoreToReach.HasValue)
            {
                foreach (ISnake s in snakes)
                {
                    if (s.IsAlive() && s.GetPlayer().GetScore() >= scoreToReach.Value)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool CheckSnakeLength(List<ISnake> snakes)
        {
            if (snakeLength.HasValue)
            {
                foreach (ISnake s in snakes)
                {
                    if (s.IsAlive() && s.GetBodyParts().Count >= snakeLength.Value)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool CheckTime(long time)
        {
            if (timeToReach.HasValue)
            {
                if (timeGoesForward)
                {
                    return timeToReach.Value <= time;
                }
                else
                {
                    return timeToReach.Value >= time;
                }
            }
            return false;
        }
    }
}
