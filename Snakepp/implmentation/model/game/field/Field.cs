﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.field
{
    
    class Field : IField

    {
        public int Width { get; }
        public int Height { get; }
        private List<IItem> items;
        private List<IWall> walls;
        private List<IBodyPart> bodyParts;

        private List<ISnake> snakes;
        private List<Thread> threads;

        private List<IItem> removedItems;

        private bool begun;
        
        public Field(Point dimensions)
        {
            if (dimensions.X <= 0 || dimensions.Y <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            items = new List<IItem>();
            walls = new List<IWall>();
            bodyParts = new List<IBodyPart>();
            snakes = new List<ISnake>();
            removedItems = new List<IItem>();
            threads = new List<Thread>();

            begun = false;

            this.Width = dimensions.X;
            this.Height = dimensions.Y;
        }

        private bool contains(ICollidable item)
        {
            Point coord = item.GetPoint();
            return (coord.X >= 0 && coord.X < this.Width && coord.Y >= 0 && coord.Y < this.Height);
        }

        private bool isCollidableAddable<T>(T item, List<T> list) where T: ICollidable
        {
            if (list.Contains(item))
            {
                return false;
            }
            if (this.contains(item))
            {
                return true;
            }
            throw new ArgumentOutOfRangeException();
        }

        private void addThread(ThreadStart method)
        {
            Thread thread = new Thread(method);
            thread.Start();
            threads.Add(thread);
        }

        public bool AddBodyPart(IBodyPart bodyPart)
        {
            if (isCollidableAddable(bodyPart, bodyParts))
            {
                bodyParts.Add(bodyPart);
                return true;
            }
            return false;
        }

        public bool AddItem(IItem item)
        {
            if (isCollidableAddable(item, items))
            {
                items.Add(item);
                if (begun)
                {
                    addThread(item.Run);
                }
                return true;
            }
            return false;
        }

        public bool AddSnake(ISnake snake)
        {
            if (snakes.Contains(snake))
            {
                return false;
            }
            snakes.Add(snake);
            return true;
        }

        public bool AddWall(IWall wall)
        {
            if (isCollidableAddable(wall, walls))
            {
                walls.Add(wall);
                return true;
            }
            return false;
        }

        public void Begin()
        {
            snakes.ForEach(snake => addThread(snake.Run));
            items.ForEach(item => addThread(item.Run));

            begun = true;
        }

        public List<IBodyPart> BodyParts
        {
            get { return new List<IBodyPart>(bodyParts); }
        }

        public List<ICollidable> GetCell(Point point)
        {
            if (point.X < 0 || point.X >= this.Width || point.Y < 0 || point.Y >= this.Height)
            {
                return new List<ICollidable>();
            }
            List<ICollidable> content = new List<ICollidable>();
            items.Where(i => i.GetPoint().Equals(point)).ToList().ForEach(i => content.Add(i));
            walls.Where(w => w.GetPoint().Equals(point)).ToList().ForEach(w => content.Add(w));
            bodyParts.Where(b => b.GetPoint().Equals(point)).ToList().ForEach(b => content.Add(b));
            return content;
        }

        public List<IItem> EliminatedItems
        {
            get
            {
                List<IItem> returnList = new List<IItem>(removedItems);
                removedItems.Clear();
                return returnList;
            }
        }

        public List<IItem> Items
        {
            get { return new List<IItem>(items); }
        }

        public List<ISnake> Snakes
        {
            get { return new List<ISnake>(snakes); }
        }

        public List<IWall> Walls
        {
            get { return new List<IWall>(walls); }
        }

        public bool RemoveBodyPart(IBodyPart bodyPart)
        {
            return bodyParts.Remove(bodyPart);
        }

        public bool RemoveItem(IItem item)
        {
            return items.Remove(item);
        }

        public ISnake RemoveSnake(int i)
        {
            ISnake snake = snakes[i];
            snake.GetBodyParts().ForEach(b => bodyParts.Remove(b));
            snakes.Remove(snake);
            return snake;
        }
    }
}
