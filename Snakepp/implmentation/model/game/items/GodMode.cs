﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class GodMode : EffectAbstract
    {
        public GodMode(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target) { }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetProperties().GetCollisionProperty().SetInvincibility(false);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake)
        {
            snake.GetProperties().GetCollisionProperty().SetInvincibility(true);
        }
    }
}
