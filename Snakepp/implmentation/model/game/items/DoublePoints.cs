﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class DoublePoints : EffectAbstract
    {
        public static readonly double MULTIPLIER = 2;

        public DoublePoints(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target) { }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetPlayer().ApplyScoreMultiplier(snake.GetPlayer().GetScoreMultiplier() / MULTIPLIER);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake)
        {
            snake.GetPlayer().ApplyScoreMultiplier(snake.GetPlayer().GetScoreMultiplier() * MULTIPLIER);
        }
    }
}
