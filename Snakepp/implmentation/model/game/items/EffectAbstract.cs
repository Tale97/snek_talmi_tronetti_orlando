﻿using Snakepp.design.model.game;
using System;
using System.Threading;

namespace Snakepp.implmentation.model.game.items
{
    abstract class EffectAbstract : IEffect
    {
        private Nullable<long> dEffectDuration;
        private ISnake attachedSnake;
        private int counter;

        protected EffectAbstract(Nullable<long> dEffectDuration)
        {
            this.dEffectDuration = dEffectDuration;
            attachedSnake = null;
            counter = 1;
        }
        public void AttachSnake(ISnake snake)
        {
            attachedSnake = snake;
        }

        public ISnake GetAttachedSnake()
        {
            return attachedSnake;
        }

        public Nullable<long> GetEffectDuration()
        {
            return dEffectDuration;
        }

        public abstract void ExpirationEffect(IField field);

        public int GetComboCounter()
        {
            return counter;
        }

        public void IncrementDuration(long duration)
        {
            ++counter;
            this.dEffectDuration = dEffectDuration.Value + duration;
        }

        public abstract void InstantaneousEffect(ISnake target);

        protected abstract void BehaviorOnLastingEffectStart(ISnake snake);

        protected abstract void BehaviorOnLastingEffectEnd(ISnake snake);

        public void Run()
        {
            if (attachedSnake == null)
            {
                throw new Exception();
            }
            BehaviorOnLastingEffectStart(attachedSnake);
            long activationTime = (long)(new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
            long timeToWait = dEffectDuration.Value;
            while (true)
            {
                Thread.Sleep((int)timeToWait);
                long enlapsedTime = (long)(new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds - activationTime;
                if (enlapsedTime >= dEffectDuration.Value)
                {
                    break;
                } 
                else
                {
                    timeToWait = dEffectDuration.Value - enlapsedTime;
                }
            }
            attachedSnake.RemoveEffect(this);
            BehaviorOnLastingEffectEnd(attachedSnake);
        }
    }
}
