﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class Slug : EffectAbstract
    {
        private double delta;

        public Slug(Nullable<long> dEffectDuration) : base(dEffectDuration)
        {
            delta = 0;
        }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target) { }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetProperties().GetSpeedProperty().ApplySpeedMultiplier(-delta);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake)
        {
            delta = (snake.GetProperties().GetSpeedProperty().GetSpeedMultiplier() * Turbo.SPEED_MULTIPLICATOR) - snake.GetProperties().GetSpeedProperty().GetSpeedMultiplier();
            snake.GetProperties().GetSpeedProperty().ApplySpeedMultiplier(delta);
        }
    }
}
