﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class ScoreIncrement : EffectAbstract
    {
        public static readonly int SCORE_INCREMENT = Apple.SCORE_INCREMENT * 2;

        public ScoreIncrement(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target)
        {
            int counter = 0;
            foreach (IEffect e in target.GetEffects())
            {
                if (e is ScoreIncrement)
                {
                    counter = e.GetComboCounter();
                }
                target.GetPlayer().AddScore(SCORE_INCREMENT * (counter + 1));
            }
        }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake) { }

        protected override void BehaviorOnLastingEffectStart(ISnake snake) { }
    }
}
