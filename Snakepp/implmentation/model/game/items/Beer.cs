﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class Beer : EffectAbstract
    {
        public Beer(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target)
        {
            Direction direction = target.GetProperties().GetDirectionProperty().GetDirection();
            Random rnd = new Random();
            bool directionGuess = (rnd.Next() % 2) == 0;
            if (direction == Direction.UP || direction == Direction.DOWN)
            {
                if (directionGuess)
                {
                    target.GetProperties().GetDirectionProperty().ForceDirection(Direction.LEFT);
                }
                else
                {
                    target.GetProperties().GetDirectionProperty().ForceDirection(Direction.RIGHT);
                }
            }
            else
            {
                if (directionGuess)
                {
                    target.GetProperties().GetDirectionProperty().ForceDirection(Direction.UP);
                }
                else
                {
                    target.GetProperties().GetDirectionProperty().ForceDirection(Direction.DOWN);
                }
            }
        }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetProperties().GetDirectionProperty().SetReverseDirection(false);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake)
        {
            snake.GetProperties().GetDirectionProperty().SetReverseDirection(true);
        }
    }
}
