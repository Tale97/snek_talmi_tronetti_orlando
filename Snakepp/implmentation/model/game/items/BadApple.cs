﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class BadApple : EffectAbstract
    {
        public static readonly int SHORTEN_DENOMINATOR = 2;

        public BadApple(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target)
        {
            target.GetProperties().GetLengthProperty().Shorten(target.GetProperties().GetLengthProperty().GetLength() / SHORTEN_DENOMINATOR);
            target.GetPlayer().ReduceScore(target.GetPlayer().GetScore() / SHORTEN_DENOMINATOR);
        }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            int length = snake.GetProperties().GetLengthProperty().GetLength();
            snake.GetProperties().GetLengthProperty().Lengthen((length * (GetComboCounter() + 1)) - length);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake) { }
    }
}
