﻿using Snakepp.design.model.game;
using System;
using System.Drawing;

namespace Snakepp.implmentation.model.game.items
{
    class ItemFactory
    {
        private readonly IField field;

        public IItem createItem(Point point, Type effectType, Nullable<long> dExpire, Nullable<long> effectDuration)
        {
            return new Item(field, point, effectType, dExpire, effectDuration);
        }
    }
}
