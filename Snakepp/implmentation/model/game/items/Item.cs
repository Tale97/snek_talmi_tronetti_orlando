﻿using Snakepp.design.model.game;
using Snakepp.implementation.model.game.items;
using System;
using System.Drawing;
using System.Threading;

namespace Snakepp.implmentation.model.game.items
{
    class Item : CollidableAbstract, IItem
    {
        private bool eaten;
        private readonly IField field;
        private readonly Nullable<long> dExpire, dEffectDuration;
        private readonly Type effectType;

        protected Item(IField field, Point point, Type effectType,
            Nullable<long> dExpire, Nullable<long> dEffectDuration) : base(point)
        {
            field.AddItem(this);
            this.field = field;
            this.dExpire = dExpire;
            this.dEffectDuration = dEffectDuration;
            this.effectType = effectType;
            eaten = false;
        }

        public Type GetEffectType()
        {
            return effectType;
        }

        private bool IsEaten(ISnake collider)
        {
            return !collider.GetProperties().GetCollisionProperty().IsIntangible()
                    || (collider.GetProperties().GetCollisionProperty().IsIntangible());
        }

        public override void OnCollision(ISnake collider)
        {
            if (IsEaten(collider))
            {
                eaten = true;
                IEffect effect = (IEffect)Activator.CreateInstance(null, effectType.ToString());
                effect.InstantaneousEffect(collider);
                if (dEffectDuration.HasValue)
                {
                    collider.AddEffect(effect);
                }
                field.RemoveItem(this);
            }
        }

        public void Run()
        {
            if (dExpire != null && dExpire.HasValue)
            {
                Thread.Sleep((int)dExpire.Value);
                if (!eaten)
                {
                    field.RemoveItem(this);
                    IEffect effect = (IEffect)Activator.CreateInstance(null, effectType.ToString());
                    effect.ExpirationEffect(field);
                }
            }
        }
    }
}
