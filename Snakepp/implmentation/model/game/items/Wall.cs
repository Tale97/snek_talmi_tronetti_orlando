﻿using Snakepp.design.model.game;
using System.Drawing;

namespace Snakepp.implementation.model.game.items
{
    class Wall : CollidableAbstract, IWall
    {
        public Wall(Point point) : base(point) { }

        public override void OnCollision(ISnake collider)
        {
            if (collider.GetProperties().GetCollisionProperty().IsSpring())
            {
                collider.Reverse();
            }
            else if (CanBeKilled(collider))
            {
                collider.Kill();
            }
        }

        private bool CanBeKilled(ISnake collider)
        {
            return !collider.GetProperties().GetCollisionProperty().IsIntangible()
                && !collider.GetProperties().GetCollisionProperty().IsInvincible();
        }
    }
}
