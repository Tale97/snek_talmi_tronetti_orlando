﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{

    class Apple : EffectAbstract
    {
        public static readonly int LENGTH_INCREMENT = 1;
        public static readonly int SCORE_INCREMENT = 10;

        public Apple(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target)
        {
            target.GetProperties().GetLengthProperty().Lengthen(LENGTH_INCREMENT);
            target.GetPlayer().AddScore(SCORE_INCREMENT);
        }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetProperties().GetLengthProperty().Shorten(snake.GetProperties().GetLengthProperty().GetLength() - 1);
            snake.GetPlayer().ReduceScore(snake.GetPlayer().GetScore());
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake) { }
    }
}
