﻿using System.Drawing;
using Snakepp.design.model.game;

namespace Snakepp.implementation.model.game.items
{
    abstract class CollidableAbstract : ICollidable
    {
        private Point point;

        protected CollidableAbstract(Point point)
        {
            this.point = point;
        }

        public abstract void OnCollision(ISnake collider);

        public Point GetPoint()
        {
            return point;
        }

        public void SetPoint(Point point)
        {
            this.point = point;
        }
    }
}
