﻿using Snakepp.design.model.game;
using System.Drawing;

namespace Snakepp.implementation.model.game.items
{
    class BodyPart : CollidableAbstract, IBodyPart
    {
        private readonly ISnake owner;
        private bool head, tail, body, top, bottom, left, right;

        public BodyPart(ISnake owner, Point point) : base (point)
        {
            this.owner = owner;
            head = tail = body = top = bottom = left = right = false;
        }

        public override void OnCollision(ISnake collider)
        {
            ICollisionProperty collisionProperty = collider.GetProperties().GetCollisionProperty();
            if (!collisionProperty.IsIntangible() && !collisionProperty.IsInvincible())
            {
                collider.Kill();
            }
        }

        public ISnake GetOwner()
        {
            return owner;
        }

        public bool IsBody()
        {
            return body;
        }

        public void SetBody(bool body)
        {
            this.body = body;
        }

        public bool IsHead()
        {
            return head;
        }

        public void SetHead(bool head)
        {
            this.head = head;
        }

        public bool IsTail()
        {
            return tail;
        }

        public void SetTail(bool tail)
        {
            this.tail = tail;
        }

        public bool IsCombinedOnBottom()
        {
            return bottom;
        }

        public void SetCombinedOnBottom(bool combined)
        {
            bottom = combined;
        }

        public bool IsCombinedOnLeft()
        {
            return left;
        }

        public void SetCombinedOnLeft(bool combined)
        {
            left = combined;
        }

        public bool IsCombinedOnRight()
        {
            return right;
        }

        public void SetCombinedOnRight(bool combined)
        {
            right = combined;
        }

        public bool IsCombinedOnTop()
        {
            return top;
        }

        public void SetCombinedOnTop(bool combined)
        {
            top = combined;
        }

    }
}
