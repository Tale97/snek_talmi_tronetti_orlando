﻿using System;
using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game.items
{
    class Magnet : EffectAbstract
    {
        public static readonly int MAGNET_RADIOUS_MULTIPLIER = 2;

        public Magnet(Nullable<long> dEffectDuration) : base(dEffectDuration) { }

        public override void ExpirationEffect(IField field) { }

        public override void InstantaneousEffect(ISnake target) { }

        protected override void BehaviorOnLastingEffectEnd(ISnake snake)
        {
            snake.GetProperties().GetPickupProperty().SetPickupRadius(snake.GetProperties().GetPickupProperty().GetPickupRadius() / MAGNET_RADIOUS_MULTIPLIER);
        }

        protected override void BehaviorOnLastingEffectStart(ISnake snake)
        {
            snake.GetProperties().GetPickupProperty().SetPickupRadius(snake.GetProperties().GetPickupProperty().GetPickupRadius() * MAGNET_RADIOUS_MULTIPLIER);
        }
    }
}
