﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class CollisionProperty : ICollisionProperty
    {

        private bool invincible;
        private bool intangible;
        private bool spring;

        public CollisionProperty()
        {
            this.invincible = false;
            this.intangible = false;
            this.spring = false;
        }
        public bool IsIntangible()
        {
            return this.intangible;
        }

        public bool IsInvincible()
        {
            return this.invincible;
        }

        public bool IsSpring()
        {
            return this.spring;
        }

        public void SetIntangibility(bool intangibility)
        {
            this.intangible = intangibility;
        }

        public void SetInvincibility(bool inv)
        {
            this.invincible = inv;
        }

        public void SetSpring(bool spring)
        {
            this.spring = spring;
        }
    }
}
