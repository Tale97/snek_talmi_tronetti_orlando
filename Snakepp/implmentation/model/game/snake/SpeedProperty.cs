﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class SpeedProperty : ISpeedProperty
    {
        const double DEFAULTLENGTHMULTIPLIER = 0;

        private long deltaT;
        private double multiplier;
        private double lenghtValue;

        public SpeedProperty(long deltaT, double speedMultiplier)
        {
            CheckDelta(deltaT);
            this.deltaT = deltaT;
            CheckMultiplier(speedMultiplier);
            this.multiplier = speedMultiplier;
            this.lenghtValue = DEFAULTLENGTHMULTIPLIER;
        }
        public void ApplyLengthSpeedValue(double val)
        {
            this.lenghtValue = val;
        }

        public void ApplySpeedMultiplier(double mul)
        {
            if (this.multiplier + mul < 0)
            {
                throw new ArgumentException();
            }
            this.multiplier += mul;
        }

        public long GetDeltaT()
        {
            return this.deltaT;
        }

        public double GetLengthSpeedValue()
        {
            return this.lenghtValue;
        }

        public double GetSpeedMultiplier()
        {
            return this.multiplier;
        }

        public void SetDeltaT(long deltaT)
        {
            CheckDelta(deltaT);
            this.deltaT = deltaT;
        }

        private void CheckDelta(long deltaT)
        {
            if (deltaT <= 0)
            {
                throw new ArgumentException();
            }
        }
        private void CheckMultiplier(double multiplier)
        {
            if (multiplier < 0)
            {
                throw new ArgumentException();
            }
        }
    }
}
