﻿using Snakepp.design.model.game;
using System;

namespace Snakepp.implmentation.model.game.snake
{
    class Properties : IProperties
    {
        private ILengthProperty length;
        private IDirectionProperty direction;
        private IPickupProperty pickup;
        private ICollisionProperty collision;
        private ISpeedProperty speed;

        public Properties(Direction direction, long deltaT, double speedMultiplier)
        {
            this.length = new LengthProperty();
            this.direction = new DirectionProperty(direction);
            this.pickup = new PickupProperty();
            this.collision = new CollisionProperty();
            this.speed = new SpeedProperty(deltaT, speedMultiplier);
        }
        public ICollisionProperty GetCollisionProperty()
        {
            return this.collision;
        }

        public IDirectionProperty GetDirectionProperty()
        {
            return this.direction;
        }

        public ILengthProperty GetLengthProperty()
        {
            return this.length;
        }

        public IPickupProperty GetPickupProperty()
        {
            return this.pickup;
        }

        public ISpeedProperty GetSpeedProperty()
        {
            return this.speed;
        }
    }
}
