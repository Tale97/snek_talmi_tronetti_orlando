﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class LengthProperty : ILengthProperty
    {
        private const int LENGTH = 1;

        private int length;

        public LengthProperty()
        {
            this.length = LENGTH;
        }

        public int GetLength()
        {
            return this.length;
        }

        public void Lengthen(int n)
        {
            CheckLength(n);
            this.length += n;
        }

        public void Shorten(int n)
        {
            CheckLength(n);
            if (this.length - n > 1)
            {
                this.length -= n;
            } else
            {
                this.length = LENGTH;
            }
        }
        private void CheckLength(int n)
        {
            if (n < 0)
            {
                throw new ArgumentException();
            }
        }
    }
}
