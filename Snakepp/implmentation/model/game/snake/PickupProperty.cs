﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class PickupProperty : IPickupProperty
    {
        private const int RADIUS = 1;

        private int radius;

        public PickupProperty()
        {
            this.radius = RADIUS;
        }
        public int GetPickupRadius()
        {
            return this.radius;
        }

        public void SetPickupRadius(int radius)
        {
            CheckRadius(radius);
            this.radius = radius;
        }

        private void CheckRadius(int radius)
        {
            if (radius < 1)
            {
                throw new ArgumentException();
            }
        }
    }
}
