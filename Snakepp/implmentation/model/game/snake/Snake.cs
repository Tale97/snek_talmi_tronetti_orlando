﻿using Snakepp.design.model.game;
using Snakepp.implementation.model.game.items;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;

namespace Snakepp.implmentation.model.game.snake
{
    class Snake : ISnake
    {
        private const int SPEEDWITHLENGHTMUL = 1;
        private const long MAXTIMETOWAIT = 400;
        private const long MINTIMETOWAIT = 40;

        private readonly Player player;
        private readonly Properties properties;
        private readonly IField field;
        private readonly List<IEffect> effects;
        private readonly List<IBodyPart> bodyPart;
        private bool isLiving;
        private bool moved;
        private bool hasReversed;

        public Snake(PlayerNumber playerNumber, String playerName, Direction direction,
            long deltaT, double speedMultiplier, IField field, List<Point> point)
        {
            this.player = new Player(playerNumber, playerName);
            this.properties = new Properties(direction, deltaT, speedMultiplier);
            this.effects = new List<IEffect>();
            this.field = field;
            this.bodyPart = new List<IBodyPart>();
            this.isLiving = true;
            this.moved = false;
            this.hasReversed = false;

            for (int i = point.Count - 1; i >= 0; i++)
            {
                InsertNewHead(point[i]);
            }
            properties.GetLengthProperty().Lengthen(point.Count - 1);

        }

        public void Run()
        {
            bool lastMovementSettedNextDirection = false;
            while (this.isLiving)
            {
                WaitToMove();
                if (lastMovementSettedNextDirection)
                {
                    GetProperties().GetDirectionProperty().ForceDirection(GetProperties().GetDirectionProperty().GetNextValidDirection());
                }
                Direction currentDirection = this.properties.GetDirectionProperty().GetDirection();
                HandleCollisions(ObtainNextPoint());
                Direction nextDirection = this.properties.GetDirectionProperty().GetDirection();
                //if the property hasReversed has been activated, snake's move to the next point and then reverse his body
                if (this.hasReversed)
                {
                    Move(ObtainNextPoint());
                    Reverse();
                    this.hasReversed = false;
                    //if snake current and next direction are not the same, i want him to actually move on the item
                    //and then change direction
                }
                else if (!currentDirection.Equals(nextDirection))
                {
                    this.properties.GetDirectionProperty().ForceDirection(currentDirection);
                    Move(ObtainNextPoint());
                    this.properties.GetDirectionProperty().ForceDirection(nextDirection);
                }
                else
                { 
                    //if anything has happened snake just move to the next point 
                    Move(ObtainNextPoint());
                }
                if (GetProperties().GetDirectionProperty().HasNextValidDirection())
                {
                   lastMovementSettedNextDirection = true;
                }
                else
                {
                    this.properties.GetDirectionProperty().AllowChangeDirection();
                    lastMovementSettedNextDirection = false;
                }
            }
                //clear the field from snake body parts when snake is not alive anymore
                FieldCleaningFromDeadSnake();
            }


        //TODO dont know how to do it
            public void AddEffect(IEffect effect)
            {
            IEffect activeEffect = effect;
            bool isPresent = false;
            foreach (IEffect e in this.effects){
                if (effect.GetType().Equals(e.GetType()))
                {
                    isPresent = true;   
                }
                if (!isPresent) { 
                    this.effects.Add(e);
                    effect.AttachSnake(this);
                    Thread t = new Thread(new ThreadStart(effect.Run));
                    t.Start();
                }
                else
                {          
                   activeEffect.IncrementDuration(effect.GetEffectDuration().Value) ;
                }
            }
            
        }

        public List<IBodyPart> GetBodyParts()
        {
            return new List<IBodyPart>(this.bodyPart);
        }

        public List<IEffect> GetEffects()
        {
            return new List<IEffect>(this.effects);
        }

        public IPlayer GetPlayer()
        {
            return this.player;
        }

        public IProperties GetProperties()
        {
            return this.properties;
        }

        public bool HasMoved()
        {
            if (this.moved)
            {
                this.moved = false;
                return true;
            }
            return false;
        }

        public bool IsAlive()
        {
            return this.isLiving;
        }

        public void Kill()
        {
            this.isLiving = false;
        }

        public bool RemoveEffect(IEffect effect)
        {
            return this.effects.Remove(effect);
        }

        public void Reverse()
        {
            if (this.hasReversed)
            {
                Direction direction;
                int snakeSize = this.bodyPart.Count;

                if (snakeSize > 1)
                {
                    Point p1 = this.bodyPart[snakeSize - 1].GetPoint();
                    Point p2 = this.bodyPart[snakeSize - 2].GetPoint();
                    direction = DeterminateDirection(p1,p2);
                    List<IBodyPart> tmp = new List<IBodyPart>(this.bodyPart);
                    this.bodyPart.Clear();
                    for (int i = 0; i <= tmp.Count -1; i++)
                    {
                        this.field.RemoveBodyPart(tmp[i]);
                        InsertNewHead(tmp[i].GetPoint());
                    }
                } else
                {
                    direction = DeterminateOppositeDirection(this.properties.GetDirectionProperty().GetDirection());
                }
                this.properties.GetDirectionProperty().ForceDirection(direction);
            }
            this.hasReversed = true;
        }

        private Direction DeterminateOppositeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.UP: return Direction.DOWN;
                case Direction.DOWN: return Direction.UP;
                case Direction.RIGHT: return Direction.LEFT;
                case Direction.LEFT: return Direction.RIGHT;
                default: throw new InvalidOperationException();
            }
        }

        private Direction DeterminateDirection(Point p1, Point p2)
        {
            //if x are the same Snake is moving up or down
            if (p1.X == p2.X)
            {
                if (p1.Y > p2.Y)
                {
                    if (p1.Y - p2.Y == 1)
                    {
                        return Direction.DOWN;
                    }
                    else
                    {
                        return Direction.UP;
                    }
                }
                else
                {
                    if (p1.Y - p2.Y == -1)
                    {
                        return Direction.UP;
                    }
                    else
                    {
                        return Direction.DOWN;
                    }
                }
            }
            //in this case Snake is moving right or left
            if (p1.Y == p2.Y)
            {
                if (p1.X > p2.X)
                {
                    if (p1.X - p2.X == 1)
                    {
                        return Direction.RIGHT;
                    }
                    else
                    {
                        return Direction.LEFT;
                    }
                }
                else
                {
                    if (p1.X - p2.X == -1)
                    {
                        return Direction.LEFT;
                    }
                    else
                    {
                        return Direction.RIGHT;
                    }
                }
            }
            throw new InvalidOperationException();
        }

        private void InsertNewHead(Point point)
        {
            int size = this.bodyPart.Count();
            IBodyPart p = new BodyPart(this, point);

            if (size == 0)
            {
                p.SetHead(true);
                p.SetTail(true);
                //if the size is 1, i'm adding a new head, and the old one is not the head anymore, but only the tail
            }
            else if (size == 1)
            {
                p.SetHead(true);
                this.bodyPart[0].SetHead(false);
                BodyPartCombination(p);
                //in all the other case, i'm adding the new and the old one is not head anymore, but it is a body part
            }
            else
            {
                p.SetHead(true);
                BodyPartCombination(p);
                this.bodyPart[0].SetHead(false);
                this.bodyPart[0].SetBody(true);
            }
            this.bodyPart.Insert(0,p);
            this.field.AddBodyPart(p);
        }

        private void RemoveTail()
        {
            if (this.bodyPart.Count > 1)
            {
                int last = this.bodyPart.Count - 1;
                this.field.RemoveBodyPart(this.bodyPart[last]);
                IBodyPart oldTail = bodyPart[last];
                bodyPart.RemoveAt(last);
                last--;
                this.bodyPart[last].SetTail(true);
                this.bodyPart[last].SetBody(false);
                switch (DeterminateDirection(oldTail.GetPoint(), this.bodyPart[last].GetPoint()))
                {
                    case Direction.UP:
                        this.bodyPart[last].SetCombinedOnTop(false);
                        break;
                    case Direction.DOWN:
                        this.bodyPart[last].SetCombinedOnBottom(false);
                        break;
                    case Direction.LEFT:
                        this.bodyPart[last].SetCombinedOnLeft(false);
                        break;
                    case Direction.RIGHT:
                        this.bodyPart[last].SetCombinedOnRight(false);
                        break;
                    default: throw new InvalidOperationException();
                }
            }
        }

        private void BodyPartCombination(IBodyPart part)
        {
            switch (DeterminateDirection(this.bodyPart[0].GetPoint(), part.GetPoint()))
            {
                case Direction.UP:
                    this.bodyPart[0].SetCombinedOnBottom(true);
                    part.SetCombinedOnTop(true);
                    break;
                case Direction.DOWN:
                    this.bodyPart[0].SetCombinedOnTop(true);
                    part.SetCombinedOnBottom(true);
                    break;
                case Direction.LEFT:
                    this.bodyPart[0].SetCombinedOnRight(true);
                    part.SetCombinedOnLeft(true);
                    break;
                case Direction.RIGHT:
                    this.bodyPart[0].SetCombinedOnLeft(true);
                    part.SetCombinedOnRight(true);
                    break;
                default: throw new InvalidOperationException();
            }
        }

        private Point ObtainNextPoint()
        {
            Point next = new Point();
            Point head = this.bodyPart[0].GetPoint();
            switch (this.properties.GetDirectionProperty().GetDirection())
            {
                case Direction.UP: next = new Point(head.X, head.Y - 1); break;
                case Direction.DOWN: next = new Point(head.X, head.Y + 1); break;
                case Direction.LEFT: next = new Point(head.X - 1, head.Y); break;
                case Direction.RIGHT: next = new Point(head.X + 1, head.Y); break;
                default: throw new InvalidOperationException();
            }
            //I check if the snake head is going to move out of the field border
            //and i set where he have to move in each case
            if (next.X < 0)
            {
                next = new Point(this.field.GetWidth() - 1, next.Y);
            }
            else if (next.X >= this.field.GetWidth())
            {
                next = new Point(0, next.Y);
            }
            else if (next.Y < 0)
            {
                next = new Point(next.X, this.field.GetHeight() - 1);
            }
            else if (next.Y >= this.field.GetHeight())
            {
                next = new Point(next.X, 0);
            }
            return next;
        }

        private void Move(Point next)
        {
            ChangeSpeedWithLenght();
            InsertNewHead(next);
            while (this.bodyPart.Count() > this.properties.GetLengthProperty().GetLength())
            {
                RemoveTail();
            }
            this.moved = true;
        }

        private void WaitToMove()
        {
            long startingTime = (long)(new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
            long timeToWait = (long)((properties.GetSpeedProperty().GetDeltaT() + this.properties.GetSpeedProperty().GetLengthSpeedValue())
                    * properties.GetSpeedProperty().GetSpeedMultiplier());
            //timeToWait must be a value between max and minimum, snake can not go faster or slower
            if (timeToWait > MAXTIMETOWAIT)
            {
                timeToWait = MAXTIMETOWAIT;
            }
            else if (timeToWait < MINTIMETOWAIT)
            {
                timeToWait = MINTIMETOWAIT;
            }
            while (true)
            {
                Thread.Sleep((int)timeToWait);
                long deltaT = (long)(new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds - startingTime;
                if (deltaT >= timeToWait)
                {
                    break;
                }
                else
                {
                    startingTime = (long)(new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                    timeToWait -= deltaT;
                }
            }
        }

        private void HandleCollisions(Point next)
        {
            List<ICollidable> cellContent = new List<ICollidable>(GetItemToCollide(next));
            foreach (ICollidable c in this.field.GetCell(next))
            {
                //if in the next cell there is snake's tail, he has to collide only if the tail is going to stay in the same cell
                //in the next movement, and this happen only if snake's supposed length is bigger than snake's actual length on the screen
                if (c is IBodyPart && c.GetPoint().Equals(this.bodyPart[this.bodyPart.Count() - 1].GetPoint())) {
                    if (this.properties.GetLengthProperty().GetLength() > this.bodyPart.Count())
                    {
                    c.OnCollision(this);
                    }
                } 
                else
                {
                //if it is not a body part you have to collide with it in any case 
                c.OnCollision(this);
                }
            }
            foreach (ICollidable collidable in cellContent) {
                if (collidable is IItem) {
                    collidable.OnCollision(this);
                }
            }
        }

        private  List<ICollidable> GetItemToCollide(Point point)
        {
            List<Point> cells = new List<Point>();
            List<ICollidable> item = new List<ICollidable>();
            List<Point> tmp = new List<Point>();

            cells.Add(point);
            //i add the first point to collide with, and then in cycle i determinate all the 
            //point adjacent to this one and I add the to the list. Every time i determinate
            //all the adjacent point of all the point in the list
            for (int i = 1; i < this.properties.GetPickupProperty().GetPickupRadius(); i++)
            {
                foreach (Point c in cells)
                {
                    tmp.AddRange(GetAdjacentPoints(c));
                }
                cells.AddRange(tmp);
                tmp.Clear();
            }
            //Snake do not have to collide more than once to the same item in the same cell
            foreach (Point c in cells)
            {
                for (int i = 0; i < this.field.GetCell(c).Count(); i++)
                {
                    if (!c.Equals(point))
                    {
                        item.Add(this.field.GetCell(c)[i]);
                    }
                }
            }
            return item;
        }

        private List<Point> GetAdjacentPoints(Point point)
        {
            List<Point> adjacentPoints = new List<Point>();
            if (Math.Abs(point.X - (point.X + 1)) == 1)
            {
                AddNotPresentPoint(adjacentPoints, new Point(point.X + 1, point.Y));
                if (Math.Abs(point.Y - (point.Y + 1)) == 1)
                {
                    AddNotPresentPoint(adjacentPoints, new Point(point.X + 1, point.Y + 1));
                    AddNotPresentPoint(adjacentPoints, new Point(point.X, point.Y + 1));
                }
                if (Math.Abs(point.Y - (point.Y - 1)) == 1)
                {
                    AddNotPresentPoint(adjacentPoints, new Point(point.X + 1, point.Y - 1));
                    AddNotPresentPoint(adjacentPoints, new Point(point.X, point.Y - 1));
                }
            }
            if (Math.Abs(point.X - (point.X - 1)) == 1)
            {
                AddNotPresentPoint(adjacentPoints, new Point(point.X - 1, point.Y));
                if (Math.Abs(point.Y - (point.Y + 1)) == 1)
                {
                    AddNotPresentPoint(adjacentPoints, new Point(point.X - 1, point.Y + 1));
                }
                if (Math.Abs(point.Y - (point.Y - 1)) == 1)
                {
                    AddNotPresentPoint(adjacentPoints, new Point(point.X - 1, point.Y - 1));
                }
            }
            return adjacentPoints;
        }

        private void AddNotPresentPoint(List<Point> list, Point point)
        {
            if (!list.Contains(point))
            {
                list.Add(point);
            }
        }

        private double ChangeSpeedWithLenght()
        {
            int val;
            //if snake length is only 1, i do not have to change his speed
            if (this.properties.GetLengthProperty().GetLength() == 1)
            {
                val = 0;
                //if snake is going to increase his length, the multiplier is negative, snake has to wait
                //less time before moving
            }
            else if (this.properties.GetLengthProperty().GetLength() >= this.bodyPart.Count())
            {
                val = -this.GetProperties().GetLengthProperty().GetLength() * SPEEDWITHLENGHTMUL;
            }
            else
            {
                val = this.GetProperties().GetLengthProperty().GetLength() * SPEEDWITHLENGHTMUL;
            }
            this.properties.GetSpeedProperty().ApplyLengthSpeedValue(val);
            return val;
        }

        private void FieldCleaningFromDeadSnake()
        {
            foreach (IBodyPart bodyPart in this.bodyPart)
            {
                this.field.RemoveBodyPart(bodyPart);
            }
            this.bodyPart.Clear();
        }
    }
}
