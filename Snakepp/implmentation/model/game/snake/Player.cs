﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class Player : IPlayer
    {
        private const int MULTIPLIER = 1;
        private const int SCORE = 0;

        private readonly PlayerNumber playerNumber;
        private readonly String playerName;
        private int score;
        private double scoreMultiplier;

        public Player(PlayerNumber playerNumber, String playerName)
        {
            //this check is useless in c#
            //CheckPlayerNumber(playerNumber);
            this.playerNumber = playerNumber;
            CheckPlayerName(playerName);
            this.playerName = playerName;
            this.score = SCORE;
            this.scoreMultiplier = MULTIPLIER;
        }
        public void AddScore(int score)
        {
            CheckScore(score);
            this.score += (int)(score * this.scoreMultiplier);
        }

        public void ApplyScoreMultiplier(double mult)
        {
            this.scoreMultiplier = mult;
        }

        public string GetName()
        {
            return this.playerName;
        }

        public PlayerNumber GetPlayerNumber()
        {
            return this.playerNumber;
        }

        public int GetScore()
        {
            return this.score;
        }

        public double GetScoreMultiplier()
        {
            return this.scoreMultiplier;
        }

        public void ReduceScore(int score)
        {
            CheckScore(score);
            if (this.score - (score * this.scoreMultiplier) > 0)
            {
                this.score -= (int)(score * scoreMultiplier);
            }
            else
            {
                this.score = 0;
            }
        }

        private void CheckScore(int score)
        {
            if (score < 0)
            {
                throw new ArgumentException();
            }
        }

        private void CheckPlayerName(String name)
        {
            if (name == null)
            {
                throw new ArgumentNullException();
            }
        }

    }
}
