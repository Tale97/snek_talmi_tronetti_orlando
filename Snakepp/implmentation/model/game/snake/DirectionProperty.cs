﻿using Snakepp.design.model.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snakepp.implmentation.model.game.snake
{
    class DirectionProperty : IDirectionProperty
    {

        private Direction direction;
        private Direction? nextInputedDirection;
        private bool reversed;
        private bool canChangeDirection;

        public DirectionProperty(Direction direction)
        {
            //CheckDirection(direction); --useless
            this.direction = direction;
            this.reversed = false;
            this.canChangeDirection = true;
            nextInputedDirection = null;
        }
        public void AllowChangeDirection()
        {
            this.canChangeDirection = true;
        }

        public bool ForceDirection(Direction direction)
        {
            this.direction = direction;
            return true;
        }

        public Direction GetDirection()
        {
            return this.direction;
        }

        public Direction GetNextValidDirection()
        {
            if (nextInputedDirection.HasValue)
            {
                Direction result = nextInputedDirection.Value;
                nextInputedDirection = null;
                return result;
            }
            return this.direction;
        }

        public bool HasNextValidDirection()
        {
            return nextInputedDirection.HasValue;
        }

        public bool IsDirectionReversed()
        {
            return this.reversed;
        }

        public bool SetDirection(Direction direction)
        {
            //CheckDirection(direction); --useless
            if (this.canChangeDirection)
            {
                this.canChangeDirection = false;
                this.direction = DirectionCase(direction);
            } else if (!this.nextInputedDirection.HasValue)
            {
                Direction nextPossibleDirection = DirectionCase(direction);
                if (nextPossibleDirection != this.direction)
                {
                    this.nextInputedDirection = nextPossibleDirection;
                }
            }
            return this.canChangeDirection;
        }

        public void SetReverseDirection(bool reverse)
        {
            this.reversed = reverse;
        }

        private Direction DirectionCase(Direction inputDirection)
        {
            if (!this.direction.Equals(ReversedDirection(inputDirection)) && !this.reversed)
            {
                return inputDirection;
            } else if (!this.direction.Equals(inputDirection) && this.reversed)
            {
                return ReversedDirection(inputDirection);
            }
            return this.direction;
        }

        private Direction ReversedDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.UP: return Direction.DOWN;
                case Direction.DOWN: return Direction.UP;
                case Direction.LEFT: return Direction.RIGHT;
                case Direction.RIGHT: return Direction.LEFT;
                default: throw new InvalidOperationException();

            }
        }

    }
}
