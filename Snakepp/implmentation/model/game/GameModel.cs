﻿using Snakepp.design.model.game;

namespace Snakepp.implmentation.model.game
{
    class GameModel : IGameModel
    {
        private readonly IField field;
        private readonly IGameRules gameRules;

        public GameModel(IField field, IGameRules gameRules)
        {
            this.field = field;
            this.gameRules = gameRules;
        }

        public IField getField()
        {
            return field;
        }

        public IGameRules getGameRules()
        {
            return gameRules;
        }
    }
}
