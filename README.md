# snek_Talmi_Tronetti_Orlando

## TRONETTI ELISA
Le classi da me implementate in c# sono:

*  CollisionProperty
*  DirectionProperty
*  LengthProperty
*  PickupProperty
*  Player
*  Properties
*  Snake
*  SpeedProperty 

Per ogni classe ho tradotto anche la relativa interfaccia.
Le principali differenze che ho notato tra Java e C# in primo impatto sono le diverse
regole di sintassi e impostazione del codice; ho notato che rispetto agli Optional 
di Java, in C# sono utilizzati i Nullable, che trovo molto interessanti e forse anche
più intuivi a livello di utilizzo.  
Non avere la possibilità di confrontare se due oggetti fossero istanze della stessa classe è
stato complicato da gestire in c#, così come dover trovare un altro modo di gestire 
gli stream che avevo utilizzato nel codice Java. 

## TALMI ALESSANDRO
Le classi da me implementate in C# sono:

* GameModel
* ItemFactory
* CollidableAbstract
    * BodyPart
    * Item
    * Wall
* EffectAbstract
    * Apple
    * BadApple
    * Beer
    * DoublePoints
    * GhostMode
    * GodMode
    * Magnet
    * ScoreIncrement
    * ScoreLoss
    * Slug
    * Turbo
* GameRules
* ItemRule
* LossConditions
* WinConditions

E le relative interfacce IBodyPart, ICollidable, IEffect, IGameModel, IGameRules, IItem, IItemRule, ILossConditions, IWall, IWinConditions.  
Di C# ho apprezzato la sintassi compatta per richiamare il costruttore della super classe, per l'utilizzo delle reflection e per i getter e setter.  
Sotto l'aspetto thread il fatto che non ci sia necessità di implementare un interfaccia Runnable lo vedo sia come positivo sia come potenzialmente catastrofico. Positivo perchè posso potenzialmente delegare ad un altro thread più o meno qualsiasi cosa, catastrofico perché, a differenza di Java che ti obbliga ad implementare un metodo Run DEDICATO a quello scopo, questa possibilità mal gestita può risultare in una fonte incredibile di errori.  
In sintesi vedo alcuni aspetti di C# come molto interessanti, ma a discapito di una solida architettura che prevenisca il più possibile degli errori.

## NICOLA ORLANDO
La classe che ho implementato in C# è Field, insieme all'interfaccia IField.  
La cosa che mi ha colpito di più del C# è la gestione di quello che in Java sono i metodi setter e getter, implementati in C# tramite proprietà che permettono di mantenere la flessibilità di Java adottando una sintassi molto più pulita, soprattutto nel caso delle autoproprietà.  
D'altro canto, non ho apprezzato particolarmente l'equivalente dello Stream, Linq, che ha una struttura molto più improntata ai database, anche se ne riconosco l'utilità in ambienti più professionali, in applicazioni di diverso tipo o anche solo più complesse.  
La sintassi per imporre dei constraint sui generici mi è sembrata eccessivamente complicata o verbosa rispetto all'equivalente Java.